import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { Item } from './model/item';
import { selectItems, selectItemsError, selectItemsPending } from './store/items.selectors';
import { Store } from '@ngrx/store';
import { AppState } from '../../core/store';
import { Actions, ofType } from '@ngrx/effects';
import { addItem, addItemSuccess, deleteItem, loadItems } from './store/items.actions';

@Component({
  selector: 'inj-items',
  template: `
    <inj-error-msg></inj-error-msg>
    <div *ngIf="itemsError$ | async">ERRORE !!!</div>
    <div *ngIf="itemsPending$ | async">loading....</div>

    <inj-items-form
      (save)="addHandler($event)"
    ></inj-items-form>
    <inj-items-list 
      [items]="items$ | async"
      (deleteItem)="deleteHandler($event)"
    ></inj-items-list>
  `,
})
export class ItemsComponent implements OnInit {
  items$: Observable<Item[]> = this.store.select(selectItems);
  itemsError$: Observable<boolean> = this.store.select(selectItemsError);
  itemsPending$: Observable<boolean> = this.store.select(selectItemsPending);

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    // this.store.dispatch(loadItems());
  }

  deleteHandler(id: number): void {
    this.store.dispatch(deleteItem({ id }))
  }

  addHandler(item: Partial<Item>) {
    this.store.dispatch(addItem({ item }))
  }

}
