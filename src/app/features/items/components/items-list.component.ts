import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { Item } from '../model/item';

@Component({
  selector: 'inj-items-list',
  template: `
    <li *ngFor="let item of items">
      {{item.name}}
      <button (click)="deleteItem.emit(item.id)">delete</button>
    </li>
  `,
})
export class ItemsListComponent {
  @Input() items: Item[] | null = null;
  @Output() deleteItem = new EventEmitter<number>()

}
