import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Actions, ofType } from '@ngrx/effects';
import { addItemSuccess } from '../store/items.actions';
import { Item } from '../model/item';

@Component({
  selector: 'inj-items-form',
  template: `

    <form #f="ngForm" (submit)="save.emit(f.value)">
      <input type="text" ngModel name="name">
    </form>
  `,
  styles: [
  ]
})
export class ItemsFormComponent  {
  @ViewChild('f') form!: NgForm;
  @Output() save = new EventEmitter<Partial<Item>>()

  constructor( private actions$: Actions ) {
    this.actions$
      .pipe( ofType(addItemSuccess) )
      .subscribe(() => {
        this.form.reset()
      })
  }


}
