import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemsRoutingModule } from './items-routing.module';
import { ItemsComponent } from './items.component';
import { ErrorMsgComponent } from '../../core/components/error-msg.component';
import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { itemsReducer } from './store/items.reducer';
import { EffectsModule } from '@ngrx/effects';
import { ItemsEffects } from './store/items.effects';
import { ItemsListComponent } from './components/items-list.component';
import { ItemsFormComponent } from './components/items-form.component';


@NgModule({
  declarations: [
    ItemsComponent,
    ErrorMsgComponent,
    ItemsListComponent,
    ItemsFormComponent
  ],
  imports: [
    CommonModule,
    ItemsRoutingModule,
    FormsModule,
    StoreModule.forFeature('myItems', itemsReducer),
    EffectsModule.forFeature([ ItemsEffects ])

  ]
})
export class ItemsModule { }
