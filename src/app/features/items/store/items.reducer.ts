import { createReducer, on } from '@ngrx/store';
import { Item } from '../model/item';
import * as ItemsActions from './items.actions';

export interface ItemsState {
  list: Item[],
  hasError: boolean;
  pending: boolean;
}

const initialState: ItemsState = {
  list: [],
  hasError: false,
  pending: false,
}

export const itemsReducer = createReducer(
  initialState,

  on(ItemsActions.loadItems, (state, action) => {
    return { ...state, hasError: false, pending: true }
  }),
  on(ItemsActions.loadItemsSuccess, (state, action) => {
    return { list: [...action.items], hasError: false, pending: false }
  }),
  on(ItemsActions.loadItemsFail, (state, action) => {
    return { list: [], hasError: true, pending: false }
  }),

  on(ItemsActions.addItem, (state, action) => {
    return { ...state, hasError: false, pending: true }
  }),
  on(ItemsActions.addItemSuccess, (state, action) => {
    return {
      list: [...state.list, action.item],
      hasError: false, pending: false
    }
  }),
  on(ItemsActions.addItemFail, (state, action) => {
    return { ...state, hasError: true, pending: false }
  }),


  on(ItemsActions.deleteItem, (state, action) => {
    return { ...state, hasError: false, pending: true }
  }),
  on(ItemsActions.deleteItemSuccess, (state, action) => {
    return {
      list: state.list.filter(item => item.id !== action.id),
      hasError: false, pending: false
    }
  }),
  on(ItemsActions.deleteItemFail, (state, action) => {
    return { ...state, hasError: true, pending: false }
  }),
)

