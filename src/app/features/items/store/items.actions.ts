import { createAction, props } from '@ngrx/store';
import { Item } from '../model/item';

export const loadItems = createAction(
  '[items] load',
)

export const loadItemsSuccess = createAction(
  '[items] load success',
  props<{ items: Item[]}>()
)

export const loadItemsFail = createAction(
  '[items] load fail',
)



export const addItem = createAction(
  '[items] add',
  props<{ item: Partial<Item> }>()
)
export const addItemSuccess = createAction(
  '[items] add success',
  props<{ item: Item}>()
)
export const addItemFail = createAction(
  '[items] add fail'
)



export const deleteItem = createAction(
  '[items] delete',
  props<{ id: number}>()
)

export const deleteItemSuccess = createAction(
  '[items] delete success',
  props<{ id: number}>()
)

export const deleteItemFail = createAction(
  '[items] delete fail',
)
