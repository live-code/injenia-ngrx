import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  addItem, addItemFail, addItemSuccess,
  deleteItem,
  deleteItemFail,
  deleteItemSuccess,
  loadItems,
  loadItemsFail,
  loadItemsSuccess
} from './items.actions';
import { catchError, delay, filter, map, mergeMap, tap, withLatestFrom } from 'rxjs/operators';
import { Item } from '../model/item';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { createFeatureSelector, Store } from '@ngrx/store';
import { AppState } from '../../../core/store';
import {
  getSelectors,
  ROUTER_NAVIGATED,
  RouterNavigatedAction,
  routerNavigatedAction,
  RouterReducerState
} from '@ngrx/router-store';

export const selectRouter = createFeatureSelector<RouterReducerState>('router');
export const { selectUrl } = getSelectors(selectRouter)

function ofRoute(action: RouterNavigatedAction, route: string): boolean {
  return action.payload.routerState.url === route
}

@Injectable()
export class ItemsEffects {
  init$ = createEffect(() => this.actions$.pipe(
    ofType(routerNavigatedAction),
    filter((action) => ofRoute(action, '/items')),
    map(() => loadItems())
  ))

  loadItems$ = createEffect(() => this.actions$.pipe(
    ofType(loadItems),
    withLatestFrom(this.store.select(selectUrl)),
    mergeMap(
      ([action, url]) => {
        console.log('ACTION -->', url)
        return this.http.get<Item[]>('http://localhost:3000/users')
          .pipe(
            map(items => loadItemsSuccess({ items })),
            catchError(()=> of(loadItemsFail()))
          )
      }
    ),
  ));

  deleteItems$ = createEffect(() => this.actions$.pipe(
    ofType(deleteItem),
    withLatestFrom(this.store.select(selectUrl)),
    mergeMap(
      ([action, url]) => this.http.delete(`http://localhost:3000/users/${action.id}`)
        .pipe(
          map(() => {
            console.log('url', url)
            return deleteItemSuccess({ id: action.id  })
          }),
          catchError(() => of(deleteItemFail()))
        )
    )
  ))

  addItems$ = createEffect(() => this.actions$.pipe(
    ofType(addItem),
    mergeMap(
      (action) => this.http.post<Item>(`http://localhost:3000/users`, action.item)
        .pipe(
          map((item) => addItemSuccess({ item })),
          catchError(() => of(addItemFail()))
        )
    )
  ))


  constructor(
    private actions$: Actions,
    private http: HttpClient,
    private store: Store<AppState>
  ) {
  }
}
