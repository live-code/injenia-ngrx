import { createReducer, on } from '@ngrx/store';
import * as ItemsActions from './items.actions';


export interface ErrorState {
  status: 'fail' | 'success' | null,
  action: string;
}
const initialState: ErrorState = { status: null, action: ''};

export const errorReducer = createReducer(
  initialState,
  on(ItemsActions.loadItems, (state, action) => ({ status: null, action: action.type})),
  on(ItemsActions.deleteItem, (state, action) =>  ({ status: null, action: action.type})),
  on(ItemsActions.addItem, (state, action) =>  ({ status: null, action: action.type})),
  on(ItemsActions.loadItemsFail, (state, action) =>  ({ status:  'fail', action: action.type})),
  on(ItemsActions.deleteItemFail, (state, action) =>  ({ status:  'fail', action: action.type})),
  on(ItemsActions.addItemFail, (state, action) => ({ status:  'fail', action: action.type})),
  on(ItemsActions.addItemSuccess, (state, action) => ({ status:  'success', action: action.type})),
  on(ItemsActions.deleteItemSuccess, (state, action) => ({ status:  'success', action: action.type})),
  on(ItemsActions.loadItemsSuccess, (state, action) => ({ status:  'success', action: action.type})),

)
