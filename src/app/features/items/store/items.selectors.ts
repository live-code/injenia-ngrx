import { AppState } from '../../../core/store';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ItemsState } from './items.reducer';

export const getItemsFeature = createFeatureSelector<ItemsState>('myItems');

export const selectItems = createSelector(
  getItemsFeature,
  state => state.list

)
export const selectItemsError  = createSelector(
  getItemsFeature,
  state => state.hasError
)

export const selectItemsPending  = createSelector(
  getItemsFeature,
  state => state.pending
)


export const selectError = (state: AppState) => state.error;
