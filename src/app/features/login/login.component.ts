import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { login } from '../../core/store/auth/auth.actions';
import { Credentials } from '../../core/store/auth/credentials';

@Component({
  selector: 'inj-login',
  template: `
    
  <form #f="ngForm" (submit)="loginHandler(f.value)">
    <input type="text" [ngModel] name="email" placeholder="email">
    <input type="password" [ngModel] name="password" placeholder="password">
    <button type="submit">Login</button>
  </form>  
  `,
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  constructor(private store: Store) { }

  ngOnInit(): void {
  }

  loginHandler({ email, password }: Credentials) {
    this.store.dispatch(login({ email, password }));
  }
}
