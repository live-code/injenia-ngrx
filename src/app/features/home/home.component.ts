import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../core/store';
import { go } from '../../core/store/router/router.actions';

@Component({
  selector: 'inj-home',
  template: `
    <p>
      home works!
    </p>
    
    <button (click)="goto()">Go to items</button>
  `,
  styles: [
  ]
})
export class HomeComponent {

  constructor(private store: Store<AppState>) {
  }

  goto(): void {
    this.store.dispatch(go({ path: 'items'}))
  }
}
