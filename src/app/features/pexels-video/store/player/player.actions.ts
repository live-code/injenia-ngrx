import { createAction, props } from '@ngrx/store';
import { Video } from '../../model/pexels-video-response';

export const showVideo = createAction(
  '[pexels-video] show',
  props<{video: Video}>()
)
