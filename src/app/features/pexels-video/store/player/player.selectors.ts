import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PexelsVideoState } from '../../pexels-video.module';

export const getPexelsVideoFeature = createFeatureSelector<PexelsVideoState>('pexels-video');

export const getCurrentVideo = createSelector(
  getPexelsVideoFeature,
  state => state.player.currentVideo
);

export const getCurrentVideoUrl = createSelector(
  getCurrentVideo,
  state => state?.video_files[0].link || null
);


