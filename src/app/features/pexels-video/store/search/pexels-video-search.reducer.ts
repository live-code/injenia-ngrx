import { Video } from '../../model/pexels-video-response';
import { createReducer, on } from '@ngrx/store';
import * as Actions  from './pexels-video-search.actions';

export interface PexelsVideoSearchState {
  text: string;
  list: Video[];
  hasError: boolean;
  pending: boolean;
}

export const initialState: PexelsVideoSearchState = {
  text: '',
  list: [],
  hasError: false,
  pending: false,
}

export const pexelsVideoSearchReducer = createReducer(
  initialState,
  on(Actions.searchVideos, (state, action) => ({ ...state, text: action.text, list: [], pending: true, hasError: false})),
  on(Actions.searchVideosSuccess, (state, action) => ({  ...state, list: action.items, hasError: false, pending: false})),
  on(Actions.searchVideosFail, (state) => ({ ...state, hasError: true, pending: false}))
)
