import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpClient } from '@angular/common/http';
import { searchVideos, searchVideosFail, searchVideosSuccess } from './pexels-video-search.actions';
import { catchError, map, switchMap } from 'rxjs/operators';
import { PexelsVideoResponse } from '../../model/pexels-video-response';
import { of } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class PexelsVideoSearchEffects {

  searchVideoEffect$ = createEffect(() => this.actions$.pipe(
    ofType(searchVideos),
    switchMap(action => {
      return this.http.get<PexelsVideoResponse>(
        `https://api.pexels.com/videos/search?query=${action.text}&per_page=15`,
        {
          headers: {
            Authorization: '563492ad6f9170000100000189ac030285b04e35864a33b95c2838be'
          }
        }
      )
        .pipe(
          map(response => searchVideosSuccess({ items: response.videos})),
          catchError(() => of(searchVideosFail()))
        )
    })
  ))

  constructor(
    private actions$: Actions,
    private http: HttpClient
  ) {
  }
}
