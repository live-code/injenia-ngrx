import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PexelsVideoState } from '../../pexels-video.module';
import { getCurrentVideo } from '../player/player.selectors';

export const getPexelsVideoFeature = createFeatureSelector<PexelsVideoState>('pexels-video');

export const getPexelsVideosText = createSelector(getPexelsVideoFeature, state => state.search.text)
// export const getPexelsVideos = createSelector(getPexelsVideoFeature, state => state.search.list)
export const getPexelsVideos = createSelector(
  getPexelsVideoFeature,
    state => state.search.list.filter(item => item.width >= state.filters.minResolution)
)
export const getPexelsVideosSearchPending = createSelector(getPexelsVideoFeature, state => state.search.pending)

export const getPexelsVideoPicturesLame = createSelector(
  getPexelsVideos,
  getCurrentVideo,
  (videos, currentVideo) => {
    return videos.find(v => v.id === currentVideo?.id)?.video_pictures
      .map(pic => pic.picture) || []
  }
)

export const getPexelsVideoPictures = createSelector(
  getCurrentVideo,
  (state) => {
    return state?.video_pictures.map(pic => pic.picture) || []
  }
)

export const getMinRes = createSelector(
  getPexelsVideoFeature,
  state => state.filters.minResolution
)
