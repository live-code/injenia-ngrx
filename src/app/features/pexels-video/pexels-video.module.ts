import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PexelsVideoRoutingModule } from './pexels-video-routing.module';
import { PexelsVideoComponent } from './pexels-video.component';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { pexelsVideoSearchReducer, PexelsVideoSearchState } from './store/search/pexels-video-search.reducer';
import { EffectsModule } from '@ngrx/effects';
import { PexelsVideoSearchEffects } from './store/search/pexels-video-search.effects';
import { PexelsPlayerState, playerReducer } from './store/player/player.reducer';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { filtersReducer, PexelsFilterState } from './store/filters/pexels-video-filters.reducer';


export interface PexelsVideoState {
  search: PexelsVideoSearchState;
  player: PexelsPlayerState;
  filters: PexelsFilterState;
}

export const reducers: ActionReducerMap<PexelsVideoState> = {
  search: pexelsVideoSearchReducer,
  player: playerReducer,
  filters: filtersReducer
}

@NgModule({
  declarations: [
    PexelsVideoComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PexelsVideoRoutingModule,
    StoreModule.forFeature('pexels-video', reducers),
    EffectsModule.forFeature([PexelsVideoSearchEffects])
  ]
})
export class PexelsVideoModule { }
