import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Video } from './model/pexels-video-response';
import {
  getMinRes,
  getPexelsVideoPictures,
  getPexelsVideos,
  getPexelsVideosSearchPending, getPexelsVideosText
} from './store/search/pexels-video-search.selectors';
import { searchVideos } from './store/search/pexels-video-search.actions';
import { showVideo } from './store/player/player.actions';
import { getCurrentVideo, getCurrentVideoUrl } from './store/player/player.selectors';
import { FormControl } from '@angular/forms';
import { debounceTime, tap } from 'rxjs/operators';
import { filterByMinResolution } from './store/filters/pexels-video-filters.actions';

@Component({
  selector: 'inj-pexels-video',
  template: `
    <input type="text" [formControl]="input">
    <div *ngIf="pending$ | async">loading....</div>
    <hr>
    <video controls *ngIf="currentVideoUrl$ | async as video"
      [src]="video" width="300px">
      ....
    </video>
    
    <div style="display: flex">
      <div *ngFor="let pic of videosPictures$ | async">
        <img [src]="pic" width="100">
      </div>
    </div>

    <hr>
    <select [ngModel]="minRes$ | async" (change)="filterByResolution($event)">
      <option>0</option>
      <option>2000</option>
      <option>3000</option>
      <option>4000</option>
    </select>

    <div *ngFor="let video of videos$ | async">
      <img
        [src]="video.image" [alt]="'Photo by ' + video.user" width="100"
        (click)="showPreview(video)"
      >
      {{video.width}} x {{video.height}}
    </div>
  `,
})
export class PexelsVideoComponent implements OnInit {
  input = new FormControl();
  text$: Observable<string> = this.store.select(getPexelsVideosText)
  minRes$: Observable<number> = this.store.select(getMinRes)
  videos$: Observable<Video[]> = this.store.select(getPexelsVideos)
  videosPictures$: Observable<string[]> = this.store.select(getPexelsVideoPictures)
  currentVideoUrl$: Observable<string | null> = this.store.select(getCurrentVideoUrl)
  pending$: Observable<boolean> = this.store.select(getPexelsVideosSearchPending);

  constructor(private store: Store) {
    this.input.valueChanges
      .pipe(
        debounceTime(1000),
        tap(console.log)
      )
      .subscribe(text => {
        this.store.dispatch(searchVideos({ text }))
      })
  }

  ngOnInit(): void {
    this.text$.subscribe((val) => {
      this.input.setValue(val, { emitEvent: false })
    });
    this.store.dispatch(searchVideos({ text: 'nature' }))
  }

  showPreview(video: Video): void {
    this.store.dispatch(showVideo({ video }))
  }

  filterByResolution(event: Event): void {
    const minResolution = +(event.target as HTMLSelectElement).value;
    this.store.dispatch(filterByMinResolution({ minResolution }));
  }
}
