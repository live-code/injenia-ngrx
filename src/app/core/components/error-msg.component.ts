import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ErrorState } from '../../features/items/store/error.reducer';
import { selectError } from '../../features/items/store/items.selectors';
import { Store } from '@ngrx/store';
import { AppState } from '../store';
import { addItemFail, deleteItemFail } from '../../features/items/store/items.actions';


@Component({
  selector: 'inj-error-msg',
  template: `
    <div *ngIf="status$ | async as status">
      <div *ngIf="status.status === 'fail' ">
        {{  ERRORS[status.action]  }}
      </div>

      <div *ngIf="status.status === 'success' ">
        ....
      </div>
    </div>
  `,
  styles: [
  ]
})
export class ErrorMsgComponent implements OnInit {
  status$: Observable<ErrorState> = this.store.select(selectError);

  ERRORS: { [key: string]: string } = {
    [addItemFail.type]: 'Inserimento non avvenuta!!!',
    [deleteItemFail.type]: 'Cancellazione non avvenuta!!!',
  }

  constructor( private store: Store<AppState>,) { }

  ngOnInit(): void {
  }

}
