import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import * as RouterActions from './router.actions';
import { tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable()
export class RouterEffects {

  go$ = createEffect(() => this.actions$.pipe(
    ofType(RouterActions.go),
    tap(action => {
      this.router.navigateByUrl(action.path);
    })
  ), { dispatch: false })

  back$ = createEffect(() => this.actions$.pipe(
    ofType(RouterActions.back),
    tap(action => {
      this.location.back();
    })
  ), { dispatch: false })

  forward$ = createEffect(() => this.actions$.pipe(
    ofType(RouterActions.forward),
    tap(action => {
      this.location.forward();
    })
  ), { dispatch: false })

  constructor(
    private actions$: Actions,
    private router: Router,
    private location: Location
  ) {
  }
}
