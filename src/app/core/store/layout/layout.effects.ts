import { Injectable } from '@angular/core';
import { createEffect } from '@ngrx/effects';
import { fromEvent } from 'rxjs';
import { debounceTime, tap } from 'rxjs/operators';

@Injectable()
export class LayoutEffects {

  resize$ = createEffect(() => fromEvent(window, 'resize').pipe(
    debounceTime(1000),
    tap(() => console.log(window.innerHeight)),
  ), { dispatch: false});

}
