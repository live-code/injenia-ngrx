import { StoreModule, ActionReducer, MetaReducer } from '@ngrx/store';
import { AppState } from './index';
import { Auth } from './auth/auth';
import { AuthState } from './auth/auth.reducer';

export function logger(reducer: ActionReducer<any>): ActionReducer<any> {
  return function(state: AppState, action) {
    console.log('action', action);
    console.log('current state', state);

    // update state with fake data (for example taken from localstorage
    /*const result = reducer({
      ...state,
      authentication: { auth: { displayName: 'pippo'}, error: true } as AuthState
    }, action);*/
    const result = reducer(state, action)
    // console.log('next state', state.getState())
    return result;
  };
}
