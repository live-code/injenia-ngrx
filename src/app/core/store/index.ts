import { ActionReducerMap } from '@ngrx/store';
import { errorReducer, ErrorState } from '../../features/items/store/error.reducer';
import { routerReducer, RouterReducerState } from '@ngrx/router-store';
import { authReducer, AuthState } from './auth/auth.reducer';

export interface AppState {
  authentication: AuthState;
  error: ErrorState;
  router: RouterReducerState;
}

export const reducers: ActionReducerMap<AppState> = {
  authentication: authReducer,
  error: errorReducer,
  router: routerReducer
}
