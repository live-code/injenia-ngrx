import { createAction, props } from '@ngrx/store';
import { Auth } from './auth';

export const login = createAction(
  '[auth] login',
  props<{ email: string, password: string }>()
)

export const syncWithLocalStorage = createAction(
  '[auth] sync with local storage',
  props<{ auth: Auth }>()
)

export const loginSuccess = createAction(
  '[auth] login success',
  props<{ auth: Auth }>()
)

export const loginFail = createAction('[auth] login fail');

export const logout = createAction('[auth] logout');
