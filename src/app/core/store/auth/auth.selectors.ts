import { AppState } from '../index';

export const getIsAdmin = (state: AppState) => !!state.authentication.auth && state.authentication.auth.role === 'admin';
export const getIsLogged = (state: AppState) => !!state.authentication.auth;
export const getAuthToken = (state: AppState) => state.authentication.auth?.token;
export const getAuthError = (state: AppState) => state.authentication.error;
