import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { HttpClient, HttpParams } from '@angular/common/http';
import * as AuthActions from './auth.actions';
import { catchError, exhaustMap, filter, map, mapTo, switchMap, tap } from 'rxjs/operators';
import { Auth } from './auth';
import { of } from 'rxjs';
import { go } from '../router/router.actions';
import { loginSuccess } from './auth.actions';

@Injectable()
export class AuthEffects {

  init$ = createEffect(() => this.actions$.pipe(
    ofType(ROOT_EFFECTS_INIT),
    map(() => {
      const auth = localStorage.getItem('auth');
      return auth && JSON.parse(auth);
    }),
    filter(auth => !!auth),
    map(auth => AuthActions.syncWithLocalStorage({ auth }) )
  ))

  login$ = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.login),
    exhaustMap(action => {
      const params = new HttpParams()
        .set('email', action.email)
        .set('password', action.password);

      return this.http.get<Auth>('http://localhost:3000/login', { params })
        .pipe(
          tap(val => console.log(val)),
          map(auth => AuthActions.loginSuccess({ auth })),
          /*
          // Multiple Dispatch Example
          switchMap(auth => [
            AuthActions.loginSuccess({ auth }),
            go({ path : 'home'}),
          ]),
          */
          catchError(() => of(AuthActions.loginFail()))
        )
    })
  ));

  loginSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.loginSuccess),
    tap(action => localStorage.setItem('auth', JSON.stringify(action.auth))),
    mapTo(go({ path : 'home'}))
  ))


  logout$ = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.logout),
    tap(() => localStorage.removeItem('auth')),
    mapTo(go({ path : '/login'}))
  ))



  constructor(
    private actions$: Actions,
    private http: HttpClient
  ) {
  }
}
