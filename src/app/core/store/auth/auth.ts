export interface Auth {
  role: string;
  token: string;
  displayName: string;
}
