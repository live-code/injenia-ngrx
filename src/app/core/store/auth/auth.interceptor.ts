import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { iif, Observable, of, throwError } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../index';
import { getAuthToken } from './auth.selectors';
import { catchError, first, switchMap } from 'rxjs/operators';
import { go } from '../router/router.actions';

@Injectable({ providedIn: 'root'})
export class AuthInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.store.select(getAuthToken)
      .pipe(
        first(),
       /* switchMap((tk) => {
          let cloneReq = req;
          if (tk) {
            cloneReq = req.clone({ setHeaders: { Authorization: 'Bearer ' + tk}})
          }
          return next.handle(cloneReq)
        })*/
        switchMap((tk) => {
          return iif(
            () => !!tk && req.url.includes('localhost'),
            next.handle(req.clone({ setHeaders: { Authorization: 'Bearer ' + tk}})),
            next.handle(req)
          )
        }),
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            switch(err.status) {
              case 401:
              case 404:
              default:
                this.store.dispatch(go({ path: '/login'}))
            }
          }
          return throwError(err)
        })
      )
  }

  constructor(private store: Store<AppState>) {
  }

}
