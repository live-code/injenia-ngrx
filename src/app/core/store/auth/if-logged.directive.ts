import { Directive, OnDestroy, TemplateRef, ViewContainerRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../index';
import { getIsLogged } from './auth.selectors';
import { distinctUntilChanged, filter, takeUntil, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Directive({
  selector: '[injIfLogged]'
})
export class IfLoggedDirective implements OnDestroy {
  private destroy$ = new Subject();

  constructor(
    private template: TemplateRef<any>,
    private view: ViewContainerRef,
    private store: Store<AppState>
  ) {
    store.select(getIsLogged)
      .pipe(
        distinctUntilChanged(),
        tap(() => view.clear()),
        filter(logged => logged),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        view.createEmbeddedView(template)
      })
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }


}
