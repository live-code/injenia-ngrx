import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../index';
import { getIsLogged } from './auth.selectors';
import { filter, tap } from 'rxjs/operators';
import { go } from '../router/router.actions';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(private store: Store<AppState>) {
  }
  canActivate(): Observable<boolean> {
    return this.store.select(getIsLogged)
      .pipe(
        tap(isLogged => {
          if (!isLogged) {
            this.store.dispatch(go({ path: '/login'}))
          }
        })
      )
  }
}
