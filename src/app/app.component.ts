import { Component, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { logout } from './core/store/auth/auth.actions';

@Component({
  selector: 'inj-root',
  template: `
    
    <button routerLink="login">login</button>
    <button routerLink="home">home</button>
    <button routerLink="pexels-video">video</button>
    
    <button routerLink="items" *injIfLogged>items</button>
    <button (click)="logoutHandler()" *injIfLogged>Logout</button>
    
    
    <hr>
    <router-outlet></router-outlet>
    
  `,
  styles: []
})
export class AppComponent  {

  constructor(private store: Store) {

  }

  logoutHandler() {
    this.store.dispatch(logout())
  }
}
