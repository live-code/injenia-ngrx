import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/store/auth/auth.guard';

const routes: Routes = [
  { path: 'items', canActivate: [AuthGuard], loadChildren: () => import('./features/items/items.module').then(m => m.ItemsModule) },
  { path: 'home', loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule) },
  { path: 'pexels-video', loadChildren: () => import('./features/pexels-video/pexels-video.module').then(m => m.PexelsVideoModule) },
  { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
  { path: '', redirectTo: 'login', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
